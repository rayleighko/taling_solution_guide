const debug = require('../utils/debug')('static-serve')
const fs = require('fs')
const path = require('path')

const staticServe = (req, res) => {
  const mimeType = {
    '.ico': 'image/x-icon',
    '.html': 'text/html',
    '.js': 'text/javascript',
    '.css': 'text/css',
    '.png': 'image/png',
    '.jpg': 'image/jpeg',
    '.eot': 'appliaction/vnd.ms-fontobject',
    '.ttf': 'aplication/font-sfnt'
  };

  const ext = req.url === '/' ? '.html' : path.parse(req.url).ext;
  const publicPath = path.join(__dirname, '../public')
  debug('ext:', ext)

  if (Object.keys(mimeType).includes(ext)) {
    const url = req.url === '/' ? '/index.html' : req.url;
    fs.readFile(`${publicPath}${url}`, (err, data) => {
      if (err) {
        res.statusCode = 404;
        res.end('404 Error');
        return;
      }

      res.statusCode = 200
      res.setHeader('Content-Type', mimeType[ext]);
      res.end(data)
    })
    return
  }
}

module.exports = staticServe
