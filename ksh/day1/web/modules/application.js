const http = require('http')
const debug = require('../utils/debug')('application')
const staticServe = require('./static-serve')

const Application = () => {
  const server = http.createServer((req, res) => {
    staticServe(req, res)
  });

  return {
    listen(port = 3000, hostname = '127.0.0.1', fn) {
      debug('start listen()')
      server.listen(port, hostname, fn)
    }
  }
}

module.exports = Application;
