// function add(firstArg, secondArg) {
//   return 'Func 선언' + firstArg + secondArg;
// }
// console.log(add(1, 3));

// var add = (firstArg, secondArg) => {
//   return 'Var 선언' + firstArg + secondArg;
// }
// console.log(add(3, 5));

// add = (firstArg, secondArg) => {
//   return '재할당' + firstArg + secondArg;
// }
// console.log(add(5, 7));

// let add = (firstArg, secondArg) => {
//   return 'Let 선언' + firstArg + secondArg;
// }
// console.log(add(7, 9));

// add = (firstArg, secondArg) => {
//   return '재할당' + firstArg + secondArg;
// }
// console.log(add(9, 11));

// const add = (firstArg, secondArg) => {
//   return 'Const 선언' + firstArg + secondArg;
// }
// console.log(add(11, 13));

// add = (firstArg, secondArg) => {
//   return '재할당' + firstArg + secondArg;
// }
// console.log(add(13, 15));

// var, let, const의 차이점(https://gist.github.com/LeoHeo/7c2a2a6dbcf80becaaa1e61e90091e5d) 참고.
// 키워드 정리: 정의, 재정의, 재할당, 초기화, 매개변수, 인자, 리턴값, 콘솔, 함수, 에로우 펑션
