# 1회차

트러블 슈팅 참고 자료

MDN, JavaScript 참고자료 - https://developer.mozilla.org/ko/docs/Web/JavaScript/Reference

- 설명할 내용

프로그래밍 언어는 하나의 언어 - 변수는 명사, 조건문 / 반복문 / 정의문은 동사, 함수는 문장, 파일 혹은 모듈은 문서, 프로젝트는 문서의 집합 -> 책(A라는 책의 1권, 2권, 3권과 같은 느낌), 하나의 제품(프로덕트)은 하나의 작품(분권된 책의 집합, 단편은 하나의 책이 하나의 작품임).

웹 브라우저와 웹 서버

프로토콜, ip / 도메인 주소

절대경로와 상대경로

모듈과 import / export

request와 response

라우팅
